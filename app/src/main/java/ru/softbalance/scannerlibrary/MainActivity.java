package ru.softbalance.scannerlibrary;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import ru.softbalance.scanner.ScannerListener;
import ru.softbalance.scanner.ScannerRecognizer;
import ru.softbalance.scanner.Terminator;

public class MainActivity extends AppCompatActivity implements ScannerListener {

    private ScannerRecognizer scannerRecognizer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scannerRecognizer = new ScannerRecognizer(Terminator.CRLF);
    }

    @Override
    protected void onPause() {
        super.onPause();
        scannerRecognizer.stopListenClipboard();
    }

    @Override
    protected void onResume() {
        super.onResume();
        scannerRecognizer.startListenClipboard(this, findViewById(R.id.content), this);
    }

    @Override
    public void onDecode(String decodedString) {
        Toast.makeText(this, decodedString, Toast.LENGTH_LONG).show();
    }
}

package ru.softbalance.scanner;

import android.support.annotation.StringDef;

@StringDef({Terminator.NONE, Terminator.HOR_TAB, Terminator.CRLF, Terminator.SPACE})
public @interface Terminator {
    String NONE = "";
    String HOR_TAB = "\t";
    String CRLF = "\n";
    String SPACE = " ";
}

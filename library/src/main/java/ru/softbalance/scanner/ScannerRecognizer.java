package ru.softbalance.scanner;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

public class ScannerRecognizer implements ClipboardManager.OnPrimaryClipChangedListener {

    private static final int KEYCODE_SCAN_TERMINATOR = 220;
    private static final int ACTUAL_TIMEOUT_MILLS = 900;

    @Terminator
    private String termnimator = "";

    private ClipboardManager clipboardManager;

    private String decodedString;

    private StringBuilder buffer;

    private ScannerListener listener;

    private long lastInputTime;

    public ScannerRecognizer() {
        decodedString = "";
        buffer = new StringBuilder();
    }

    public ScannerRecognizer(@Terminator String terminator) {
        decodedString = "";
        buffer = new StringBuilder();
        this.termnimator = terminator;
    }

    public String getResult() {
        return decodedString;
    }

    public boolean onInput(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            prepareBuffer();
            loadKey(keyCode, String.valueOf((char) event.getUnicodeChar()));
        }
        return false;
    }

    private void prepareBuffer() {
        if (System.currentTimeMillis() - lastInputTime > ACTUAL_TIMEOUT_MILLS) {
            clearBuffer();
        }
    }

    private void loadKey(int keyCode, String key) {
        Log.d("loadKey", keyCode + " |" + key+"|");
        if (termnimator.equals(key)) {
            onScanFinish();
        } else if (keyCode != KEYCODE_SCAN_TERMINATOR) {
            putToBuffer(key);
        }
    }

    private void putToBuffer(String key) {
        buffer.append(key);
        lastInputTime = System.currentTimeMillis();
    }

    private void extractInputFromClipboard() {
        ClipData clip = clipboardManager.getPrimaryClip();
        if (clip != null && clip.getItemCount() > 0) {
            ClipData.Item clipItem = clip.getItemAt(0);
            if (clipItem != null && clipItem.getText().length() != 0) {
                buffer.append(clipItem.getText().toString().trim());
            }
        }
    }

    private void onScanFinish() {
        decodedString = buffer.toString().replaceAll("\\p{C}", "");
        clearBuffer();
        if (listener != null && !decodedString.isEmpty()) {
            listener.onDecode(decodedString);
        }
    }

    private void clearBuffer() {
        buffer = new StringBuilder();
    }

    public void setTerminator(@Terminator String terminator) {
        termnimator = terminator;
    }

    @Override
    public void onPrimaryClipChanged() {
        clearBuffer();
        extractInputFromClipboard();
        onScanFinish();
    }

    public void startListenClipboard(Context context, final View rootView, ScannerListener listener) {
        this.listener = listener;
        clipboardManager = (ClipboardManager) context.getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.addPrimaryClipChangedListener(this);

        rootView.setFocusable(true);
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.requestFocusFromTouch();
        rootView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    rootView.requestFocusFromTouch();
                    rootView.requestFocus();
                    rootView.setSelected(true);
                }
            }
        });
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                return onInput(i, keyEvent);
            }
        });
    }

    public void stopListenClipboard() {
        listener = null;
        clipboardManager.removePrimaryClipChangedListener(this);
        clipboardManager = null;
    }
}

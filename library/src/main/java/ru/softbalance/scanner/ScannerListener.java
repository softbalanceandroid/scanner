package ru.softbalance.scanner;

public interface ScannerListener {

    void onDecode(String decodedString);

}

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* contains library used for handling signals, which come to app from usb-connected devices (scanners, nfs-readers, etc.)
* contains example of usage

### How to use in Gradle? ###

* repositories { maven { url "https://bitbucket.org/softbalanceandroid/scanner/raw/HEAD/library/maven-repo/" }}
* dependencies { compile 'ru.softbalance:scanner:0.6.0' }